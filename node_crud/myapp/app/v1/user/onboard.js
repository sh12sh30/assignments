var express = require('express');
var router = express.Router();
var swig = require('swig');
let User = require('./model');
var passport = require('passport');
var LocalStrategy = require('passport-local')
var dbcontroller = require('./controller');
var passwordHash = require('password-hash');
var dbControllerObj = new dbcontroller();
// var validate = require('./validation');
// var validateObj = new validate();
/* GET home page. */
router.get('/', async function (req, res, next) {
    res.render('login.html');
});
router.post('/login', passport.authenticate('local', { failuerRedirect: '/',}), (req, res, next) => {
    try {

        passport.authenticate('local', async (err, user) => {
            if (err) {
                res.redirect("/");
            }
            if (!user) {
                req.flash('error', 'wrong input , check your credentials');
                res.redirect("/");
            }
            if (user.email_verification == 0) {
                req.flash('error', 'your email is not verified , please verify your email');
                res.redirect("/");

            }
            else {
                req.flash('success', 'Welcome');
                res.redirect('/user');
            }

        }
        )(req, res, next);
    } catch (err) {
        console.log("erorr in login");
        res.status(400).send({ message: err.message, status: 0 });
    }
    console.log(req.user)
});
passport.use(new LocalStrategy({ usernameField: 'username', passwordField: 'password', passReqToCallback: true },
    async (req, username, password, done) => {
        try {
            let data = await User.findOne({ userName: username });

            if (!data) {
                return done(null, false);
            }
            if (!passwordHash.verify(password, data.password)) {
                return done(null, false);
            }
            console.log(data, "++++++++++====================")
            return done(null, data);
        } catch (err) {
            console.log("error in passport");
            console.log(err);
        }


    }));
passport.serializeUser(function (user, done) {
    console.log(user, '+++++++++++++++++');
    done(null, user.id);
});

passport.deserializeUser(async (id, done) => {
    let data = await User.findOne({ _id: id });
    data = JSON.parse(JSON.stringify(data));
    done(null, data);
});

router.get('/logout', function (req, res) {
    req.logout();
    res.redirect('/');
});
router.post('/save_user', function (req, res, next) {
    dbControllerObj.insertvalue(req.body);
    res.redirect('/');
});

module.exports = router;

var mongoose = require('mongoose');
class databaseConnection {
    static connection() {
        mongoose.connect('mongodb://localhost:27017/loginUser');
        var dataB = mongoose.connection;
        dataB.once('open', () => {
            console.log('connnected');
        });
        dataB.on('error', () => {
            console.log('error');
        })
    }
}
module.exports = databaseConnection;
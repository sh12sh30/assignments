var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();
mongoose.connect('mongodb://localhost/connectiontest');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('userdata');
});

module.exports = router;

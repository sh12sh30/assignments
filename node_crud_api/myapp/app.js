var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var swig = require('swig');
var dbconnection = require('./connection')
var bodyParser = require('body-parser');
var passport = require('passport');
var flash = require('connect-flash');
var expressSession = require('express-session');
dbconnection.connection();

// var indexRouter = require('./routes/index');
// var usersRouter = require('./routes/users');

var app = express();

// view engine setup
// app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'ejs');
app.engine('html', swig.renderFile);
app.set('view engine', 'html');
app.set('views', __dirname + '/views');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({ extended: true }));
// app.use('/', indexRouter);
// app.use('/users', usersRouter);
app.use(expressSession({ name: 'demo', resave: false, secret: 'mySecretKey' }));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());
app.use('/', require('./app/v1/user/onboard'));
// app.use(async (req, res, next) => {
//   try {
//     if (req.user) {
//       next();
//     }
//     else {
//       res.redirect("/");
//     }
//   } catch (err) {
//     console.log(err);
//   }
// })
app.use('*', require('./app/v1/user/middleware'));
app.use('/user', require('./app/v1/user/routes'));


// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userDetail = new Schema({
  fullname: String,
  email: {
    type: String,
    unique: true
  },
  address: String,
  userName: String,
  password: String,
  isdelete: {
    type: Number,
    default: 0
  }
});

module.exports = mongoose.model("User", userDetail);
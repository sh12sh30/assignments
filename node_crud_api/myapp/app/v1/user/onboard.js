var express = require('express');
var router = express.Router();
var swig = require('swig');
let User = require('./model');
var passport = require('passport');
var LocalStrategy = require('passport-local')
var dbcontroller = require('./controller');
var passwordHash = require('password-hash');
var dbControllerObj = new dbcontroller();
// var validate = require('./validation');
// var validateObj = new validate();
/* GET home page. */
router.get('/', async function (req, res, next) {
    res.send({staus:1,message:"please login"});
});
router.post('/login', dbControllerObj.passportLogin);

router.get('/logout', function (req, res) {
    req.logout();
    res.redirect('/');
});
router.post('/save_user', function (req, res, next) {
    dbControllerObj.insertvalue(req.body);
    res.redirect('/');
});

module.exports = router;

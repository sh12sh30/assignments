var mongoose = require('mongoose');
  var Schema = mongoose.Schema;

  var userDetail = new Schema({
    name:String,
    email: String,
    address:String,
    isdelete:{
      type:Number,
      default:0
    }
  });

  module.exports = mongoose.model("User", userDetail);
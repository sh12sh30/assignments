var express = require('express');
var router = express.Router();
var swig = require('swig');
var dbcontroller = require('./controller');
var dbControllerObj = new dbcontroller();
var validate = require('./validation');
var validateObj = new validate();
/* GET home page. */
router.get('/', async function (req, res, next) {
  let data = await dbControllerObj.fetchValue();
  res.send({staus:1,message:"data successfully loaded", data: data });
});

// router.get('/user_form', function (req, res, next) {
//   res.render('adduser.html');
// });

router.post('/',validateObj.addUserVlaidate,validateObj.validateHandler, function (req, res, next) {
  dbControllerObj.insertvalue(req.body);
  res.send('data added successfully');
});

router.get('/edit/:id', (req, res, next) => {
  dbControllerObj.get_user_data(req.params.id).then((result) => {
    res.send({ data: result });
  })
});
router.get('/view/:id', (req, res, next) => {
  dbControllerObj.view_user_data(req.params.id).then((result) => {
    res.send({result});
  })
});
router.post('/edit', async (req, res, next) => {
  await dbControllerObj.update_user_data(req.body.id, req.body)
  console.log(req.body.id);
  let data = await dbControllerObj.fetchValue();
  res.send({ data: data });
});

router.post('/edit', function (req, res, next) {
  res.render('list_user.html', { data: [req.body] });
});

router.put('/delete/:id', (req, res, next) => {
  dbControllerObj.delete_user_data(req.params.id).then((result) => {
    console.log(result);
    // res.redirect('/');
    res.send('data added successfully');
  })
});

module.exports = router;

var mongoose = require('mongoose');
let User = require('./model');
class dataController {
  async insertvalue(data) {
    User.create(data)
      .then(doc => {
        console.log(doc)
      })
      .catch(err => {
        console.error(err)
      })
  }
  async fetchValue() {
    try {

      var data = await User.find({isdelete:0});
      return Promise.resolve(data);
    }
    catch (err) {
      console.log(err)
    }

  }
  async get_user_data(id) {
    try {

      var data = await User.findOne({_id:id});
      return Promise.resolve(data);
      
    }
    catch (err) {
      console.log(err)
    }

  }
  async view_user_data(id) {
    try {

      var data = await User.findOne({_id:id});
      return Promise.resolve(data);
      
    }
    catch (err) {
      console.log(err)
    }

  }

  async update_user_data(id,data) {
    try {
      var data = await User.update({_id:id},data);
      return Promise.resolve(data);
      
    }
    catch (err) {
      console.log(err)
    }

  }
  async delete_user_data(id) {
    try {
      var data = await User.updateOne({_id:id},{$set:{isdelete:1}});
      return Promise.resolve(data);
      
    }
    catch (err) {
      console.log(err)
    }

  }

}
module.exports = dataController;
#!/bin/bash
echo "press 1 for install node&npm "
echo "press 2 for install apache2 "
echo "press 3 for install mysql-server "
echo "press 4 for install phpmyadmin"
echo "press 5 for install mongodb "
echo "press 6 for install express "
echo "press 7 for install angular-cli"
read  i
#for install node&npm
if [ $i -eq 1 ]
then
sudo apt-get install curl
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt-get install -y nodejs
echo  "node & npm installed successfully"
node -v 
npm -v 
#for install apache server 2
elif [ $i -eq 2 ]
then
   sudo apt install -y apache2 
   echo  "apache 2 installed successfully"
   apache2 -version
   sudo chmod 777 /var/www/html
   #for install mysql-server
elif [ $i -eq 3 ]
then
   sudo apt install -y mysql-server
   echo  "mysql-server installed successfully"
   #for install phpmyadmin 
elif [ $i -eq 4 ]
then
  sudo apt-get install -y phpmyadmin
  echo  "phpmyadmin installed successfully"
  #for install mongodb
elif [ $i -eq 5 ]
then
  sudo apt-get install -y mongodb
  sudo systemctl enable mongod
  sudo systemctl start mongod 
  echo  "mongodb installed successfully"
  #for install express
elif [ $i -eq 6 ]
then
  npm install -g express-generator
  echo  "express-generator installed successfully"
  #for install angular-cli
  elif [ $i -eq 7 ]
then
  npm install -g @angular/cli
  echo  "angular/cli installed successfully"
else
    echo "you entered wrong value"
fi
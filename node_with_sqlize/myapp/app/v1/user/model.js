const Sequelize = require('sequelize');
const sequelize = require('../../../connection');
let User = sequelize.define('User', {
  name: {
    type: Sequelize.STRING,
    field: 'name', // Will result in an attribute that is firstName when user facing but first_name in the database
  },
  email: {
    type: Sequelize.STRING,
    field:'email',
   
},
address: {
  type: Sequelize.STRING,
  field:'address',
 
}},{
  freezeTableName: true,
  underscored: true // Model tableName will be the same as the model name
});

sequelize.sync({ force: false }).then(()=> {
    console.log('It worked!');
  },function (err) { 
    console.log('An error occurred while creating the table:', err);
  });
module.exports = User;
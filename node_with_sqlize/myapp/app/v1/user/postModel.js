const Sequelize = require('sequelize');
const sequelize = require('../../../connection');

let Post = sequelize.define('Post', {
    PostName: {
      type: Sequelize.STRING,
      field: 'PostName', // Will result in an attribute that is firstName when user facing but first_name in the database
     
    },
     content: {
      type: Sequelize.STRING,
      required: true,
      field: 'content'
    },
    user_id: {
      type: Sequelize.INTEGER,
      field: 'user_id', // Will result in an attribute that is firstName when user facing but first_name in the database
     
    },
  },{
    freezeTableName: true,// Model tableName will be the same as the model name
    underscored:true
  });
  sequelize.sync({ force: false }).then(()=> {
      console.log('It worked!');
    //   return Post.create({
    //     PostName: 'userSecondPost',
    //     content: "More often than not, our days don’t begin peacefully and joyfully ",
    //     user_id:1
       
    //   });
    }, function (err) { 
      console.log('An error occurred while creating the table:', err);
    });
  
  module.exports = Post;
const Sequelize = require('sequelize');
const sequelize = require('../../../connection');

let Comment = sequelize.define('Comment', {
    CommentName: {
      type: Sequelize.STRING,
      field: 'CommentName', // Will result in an attribute that is firstName when user facing but first_name in the database
     
    },
     content: {
      type: Sequelize.STRING,
      required: true,
      field: 'content'
    },
    commenter_username: {
        type:Sequelize.STRING,
        required: true,
        field: 'commenter_username'
      },
      commenter_email: {
        type: Sequelize.STRING,
        required: true,
        field:'commenter_email'
      },
    post_id: {
      type: Sequelize.INTEGER,
      field:'post_id' // Will result in an attribute that is firstName when user facing but first_name in the database  
    },
  },{
    freezeTableName: true,// Model tableName will be the same as the model name
    underscored:true
  });
  sequelize.sync({ force: false }).then(()=> {
      console.log('It worked!');
      return Comment.create({
        CommentName: 'commented on post',
        content: "intellectual post and beneficial",
        commenter_username:"hazelwood",
        commenter_email:"hazel.wood@gmail.com",
        post_id:3
       
      });
    }, function (err) { 
      console.log('An error occurred while creating the table:', err);
    });
  
  module.exports = Comment;
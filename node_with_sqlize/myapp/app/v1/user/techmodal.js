const Sequelize = require('sequelize');
const sequelize = require('../../../connection');

let Tech = sequelize.define('Tech', {
    techName: {
      type: Sequelize.STRING,
      field: 'techName', // Will result in an attribute that is firstName when user facing but first_name in the database
     
    },
    salary: {
      type: Sequelize.INTEGER,
      field: 'salary', // Will result in an attribute that is firstName when user facing but first_name in the database
     
    },
    user_id: {
      type: Sequelize.INTEGER,
      field: 'user_id', // Will result in an attribute that is firstName when user facing but first_name in the database
     
    },
  },{
    freezeTableName: true,// Model tableName will be the same as the model name
    underscored:true
  });
  sequelize.sync({ force: false }).then(()=> {
      console.log('It worked!');
    }, function (err) { 
      console.log('An error occurred while creating the table:', err);
    });
  
  module.exports = Tech;
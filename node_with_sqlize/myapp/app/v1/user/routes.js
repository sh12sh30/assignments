var express = require('express');
var router = express.Router();
var swig = require('swig');
var dbcontroller = require('./controller');
var dbControllerObj = new dbcontroller();
var validate = require('./validation');
var validateObj = new validate();
/* GET home page. */
router.get('/', async function (req, res, next) {
  let data = await dbControllerObj.fetchValue();
  res.render('list_user.html', { data: data });
});

router.get('/user_form', function (req, res, next) {
  res.render('adduser.html');
});

router.post('/',  function (req, res, next) {
   dbControllerObj.insertvalue(req.body);
  res.redirect('/');
});

router.get('/edit/:id', (req, res, next) => {
  dbControllerObj.get_user_data(req.params.id).then((result) => {
    res.render('edituser.html', { data: result });
  })
});
router.get('/view/:id', (req, res, next) => {
  dbControllerObj.view_user_data(req.params.id).then((result) => {
    res.send({result});
  })
});
router.post('/edit', async (req, res, next) => {
  await dbControllerObj.update_user_data(req.body)
  let data = await dbControllerObj.fetchValue();
  res.render('list_user.html', { data: data });
});

// router.post('/edit', function (req, res, next) {
//   res.render('list_user.html', { data: [req.body] });
// });

router.get('/delete/:id', (req, res, next) => {
  dbControllerObj.delete_user_data(req.params.id).then((result) => {
    res.redirect('/');
  })
});

// routes for add user's technology
router.get('/add_technology/:id', function (req, res, next) {
    res.render('technology.html',{data:req.params.id});
});
router.get('/showTech', function (req, res, next) {
  res.send(console.log("=========+++"));
});

router.post('/showTech',  function (req, res, next) {
  dbControllerObj.insertTech(req.body);
 res.redirect('/');
//  res.send({id:"123"});
 
});

// route for show user all details
router.get('/showDetail/:id', function (req, res, next) {
  dbControllerObj.showDetail(req.params.id).then((result)=>{

    let data=JSON.parse(JSON.stringify(result))
    console.log("adfasd++++++++++++++++",data.Posts[0])
    res.render('user_detail.html',{ data:data[0]});
  })
});

module.exports = router;

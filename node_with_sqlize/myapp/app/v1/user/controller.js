let User = require('./model');
let Post = require('./postModel');
let Comment = require('./comment');
// let Tech = require('./techmodal');
// User.hasMany(Tech);
Comment.belongsTo(Post);
Post.hasMany(Comment);
Post.belongsTo(User);
User.hasMany(Post);

class dataController {
  async insertvalue(data) {
    User.create(data) .then(doc => {
        console.log(doc)
      })
      .catch(err => {
        console.error(err)
      })
  }
  async fetchValue() {
    try {

      var data = await User.findAll();
      return Promise.resolve(data);
    }
    catch (err) {
      console.log(err)
    }

  }
  async get_user_data(id) {
    try {

      var data = await User.findOne({where:{id:id}});
      return Promise.resolve(data);
      
    }
    catch (err) {
      console.log(err)
    }

  }
  async view_user_data(id) {
    try {

      var data = await User.findOne({where:{id:id}});
      return Promise.resolve(data);
      
    }
    catch (err) {
      console.log(err)
    }

  }

  async update_user_data(data) {
    try {
      var updatess = {
        name: data.name,
        email:data.email,
        address:data.address
      }

      var result = await User.update(updatess,{where:{id:data.user_id}});
      return Promise.resolve(result);
    }
    catch (err) {
      console.log(err)
    }

  }
  async delete_user_data(id) {
    try {
      var data = await User.destroy({where:{id:id}});
      return Promise.resolve(data);
      
    }
    catch (err) {
      console.log(err)
    }

  }
  // for adding user's technology
  // async AddTech(id) {
  //   try {

  //     var data = await User.findOne({where:{id:id}});
  //     return Promise.resolve(data);
      
  //   }
  //   catch (err) {
  //     console.log(err)
  //   }

  // }
  // async insertTech(techdetail) {
  //   Tech.create(techdetail) .then(doc => {
  //       console.log(doc)
  //     })
  //     .catch(err => {
  //       console.error(err)
  //     })
  // }

  async showDetail(id){
    try{
      console.log("id------",id);
   let userDetail =   User.findOne({where:{id:id},
        include:[{model:Post,
          include:[{model:Comment}],
        required:true}]
      })

       return userDetail;
   
    }catch(err){
     console.log(err);
    }
  }

}
module.exports = dataController;
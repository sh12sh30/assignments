var moment = require('moment');
// for getting date in a fromat (23-Dec-2018)
let d1 = moment([2018, 11, 23]);
console.log(d1.format('DD-MMM-YYYY'));

// for getting date in a local timezone
var d2 = moment("2014-06-01T12:00:00Z");
console.log(d2.format());


//for converting local time to utc time

let date = moment();
console.log(date.format('DD-MM-YYYY'));

let localtime = moment().format('LT');
console.log(localtime);
let dateLocalTime = moment(date).format('DD-MM-YYYY')+" "+localtime;
console.log(dateLocalTime,"show time in local");

let utctime = moment.utc(date).format('DD-MM-YYYY h:mm A');
 console.log(utctime,"converted time in utc");

 //for converting utc time to local time
 let utcTime = moment.utc().format('DD-MM-YYYY h:mm A');
 console.log(utcTime,"show time in utc");
 var Local = moment().format('YYYY-MMM-DD h:mm A');
 console.log(Local,"show time in local");

 let timeStamp = moment('2015-07-12 14:59:23', 'YYYY-MM-DD HH:mm:ss').valueOf();
  console.log(timeStamp);

//   for getting difference between two dates
let date1 = moment('2018-06-12');
let date2 = moment('2018-06-28');

let days = date2.diff(date1, 'days');
console.log(`Difference in days: ${days}`);

// for adding 30 min to a date
var addDate = moment()
    .add(30, 'seconds')
    .add(32, 'minutes')
    .format('LTS');
    console.log(addDate)
    
var mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
var Schema = mongoose.Schema;
let User = require('../Admin/model');
var userDetail = new Schema({
  PostName: String,
  Caption: {
    type: String,
    unique: true
  },
  filename:String,
  user_id:String
});

userDetail.plugin(mongoosePaginate);
module.exports = mongoose.model("UserPost", userDetail);

var express = require('express');
var router = express.Router();
var path = require('path');
var swig = require('swig');
let UserPost = require('./model');
var passport = require('passport');
var LocalStrategy = require('passport-local');
var multer = require('multer');
const MessagingResponse = require('twilio').twiml.MessagingResponse;
const accountSid = 'AC2f89c896fa138578fddc04aec1f1c251';
const authToken = 'b8c1fc908a4ae47b013813b6e2c602a5';
const client = require('twilio')(accountSid, authToken);
const configData = require("../../../config");
const stripe = require("stripe")(configData.keySecret);
const paypal = require('paypal-rest-sdk');
const fs = require('fs');
const pdf = require('html-pdf');
// const html = fs.readFileSync(path.join(__dirname,'../../../views/list_user.html'),'utf-8');
const options = {
  format: 'Letter',
  "height": "11.25in",
  "width": "8.5in",
  "header": {
    "height": "20mm"
  },
  "footer": {
    "height": "20mm",
  },
};
var dbcontroller = require('./controller');
var dbControllerObj = new dbcontroller();
// var validate = require('./validation');
// var validateObj = new validate();
/* GET home page. */

// thumb({
//   source: path.join(__dirname, '../../../public/uploaded'), // could be a filename: dest/path/image.jpg
//   destination: path.join(__dirname, '../../../public/thumbnail'),
//   concurrency: 4
// }, function(files, err, stdout, stderr) {
//   console.log('All done!');
// });



var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.join(__dirname, '../../../public/uploaded'))
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    console.log(file);
  }
})

var upload = multer({ storage: storage })

// router.post('/uploadfile', upload.single('myFile'), (req, res, next) => {
//   const file = req.file
//   if (!file) {
//     const error = new Error('Please upload a file')
//     error.httpStatusCode = 400
//     return next(error)
//   }
//   dbControllerObj.uploadCsv(req.file)
//   // console.log(req.body);
//   res.send(file)

// })

//for firebase img upload
router.post('/uploadfile',  (req, res, next) => {
  console.log("req========",req.body);
  dbControllerObj.uploadImageFirebase(req.body.myimg,req.user.email).then((result) => {
    console.log(result,"aksdjlfajsdlk33434rlkasdjflaks");

  })
  // console.log(req.body);
    res.redirect("/user")

})


router.get('/', async function (req, res, next) {

  dbControllerObj.fetchValue(req.query,req.user._id).then((data) => {
    if (!req.query.pageNo) {
      res.render('list_user_post.html', { data: data[0], showList: data[1], message: req.flash() });

    }
    else {

      console.log("enetr==========")
      var tpl = swig.compileFile(path.join(__dirname, '../../../views/pagination_list_user.html'));
      var userlist = tpl({ dataa: data[0] });

      res.send(userlist);
    }
  }).catch((err) => {
    console.log(err, "error occured")
  })

  // .then((result)=>{
  //   res.render('list_user.html', { data: result, message: req.flash() });
  //   console.log(result,"lksdjflas999999999999")
  // }).catch((err)=>{
  //   console.log(err,"error occured")
  // })

});
router.get('/chat', async function (req, res, next) {
  res.render('socketChat.html');
});


router.get('/userposts', function (req, res, next) {
  res.render('add_user_posts.html');
});
router.post('/save_post', function (req, res, next) {
  dbControllerObj.addPost(req.body,req.user);
  res.redirect('/user');
});

router.get('/edit/:id', (req, res, next) => {
  dbControllerObj.get_user_data(req.params.id).then((result) => {
    res.render('edituser.html', { data: result });
  })
});
router.get('/view/:id', (req, res, next) => {
  dbControllerObj.view_user_data(req.params.id).then((result) => {
    res.send({ result });
  })
});
router.post('/edit', async (req, res, next) => {
  await dbControllerObj.update_user_data(req.body.id, req.body)
  console.log(req.body.id);
  let data = await dbControllerObj.fetchValue();
  res.render('list_user.html', { data: data[0], showList: data[1], message: req.flash() });
});

router.post('/edit', function (req, res, next) {
  res.render('list_user.html', { data: [req.body] });
});

router.get('/delete/:id', (req, res, next) => {
  dbControllerObj.delete_user_data(req.params.id).then((result) => {
    console.log(result);
    res.redirect('/user');
  })
});

router.get('/user_profile', async function (req, res, next) {
  res.render('user_profile.html', { data: req.user });
  console.log(req.user, "akldfasdfajkshdfkh+++++++++4334325=====0000222===")
});
router.get('/exportfile', function (req, res, next) {
  dbControllerObj.exportfile().then((result) => {
    res.download(result);
  }).catch((err => {
    console.log(err)
  }));

});
router.post('/importfile', function (req, res, next) {
  dbControllerObj.importfile();
  res.send('Import CSV file using NodeJS');

})
//  for sending sms using twilio 
router.get('/sendsms', (req, res) => {
  client.messages
    .create({
      body: 'This is the message that you have recevied from my side using twilio',
      from: '+12018905898',
      to: '+917018967109'
    })
    .then(message => console.log(message.sid));
  const twiml = new MessagingResponse();

  twiml.message('The Robots are coming! Head for the hills!');
  res.send("you send an sms ")
  res.end(twiml.toString());

});
//  for creating pdf
router.get('/createpdf', function (req, res, next) {
  dbControllerObj.fetchValue().then((result) => {
    let data = JSON.parse(JSON.stringify(result));
    var tpl = swig.compileFile(path.join(__dirname, '../../../views/list_pdf_user.html'));
    var userdata = tpl({ data: data });
    pdf.create(userdata, options).toFile("report" + Date.now() + ".pdf", function (err, userdata) {
      if (err) {
        res.send(err);
        console.log(err)
      } else {
        console.log(userdata)
        req.flash('success', 'pdf crated successfully');
        res.redirect('/user');
      }
    });

  }).catch((err) => {
    console.log(err, "ksldjflsd=========")
  })



})
//  for payment gateway using stripe
router.post('/charge', function (req, res, next) {
  // var    chargeAmount  = 10*100;
  stripe.customers.create({
    email: req.body.stripeEmail, // customer email
    source: req.body.stripeToken // token for the card
  }).then(customer =>
    stripe.charges.create({ // charge the customer
      amount: 10000,
      description: "Sample Charge",
      currency: "INR",
      customer: customer.id
    })).then(charge => res.send("payment done")).catch((err) => {
      console.log(err, "error occured")
    })


})
// form payment using paypal
paypal.configure({
  'mode': 'sandbox', //sandbox or live 
  'client_id': configData.client_id, // please provide your client id here 
  'client_secret': configData.client_secret // provide your client secret here 
});

router.get('/buyWithPaypal', (req, res) => {
  // create payment object 
  var payment = {
    "intent": "authorize",
    "payer": {
      "payment_method": "paypal"
    },
    "redirect_urls": {
      "return_url": "http://localhost:3000/user/success",
      "cancel_url": "http://localhost:3000/user/err"
    },
    "transactions": [{
      "amount": {
        "total": 39.00,
        "currency": "INR"
      },
      "description": " a book on magic"
    }]
  }
  // call the create Pay method 
  createPay(payment)
    .then((transaction) => {
      var id = transaction.id;
      var links = transaction.links;
      var counter = links.length;
      while (counter--) {
        if (links[counter].method == 'REDIRECT') {
          // redirect to paypal where user approves the transaction 
          return res.redirect(links[counter].href)
        }
      }
    })
    .catch((err) => {
      console.log(err);
      res.redirect('/err');
    });

})
router.get('/success' , (req ,res ) => {
  console.log(req.query); 
  res.send('payment has done successfully'); 
})
// error page 
router.get('/err' , (req , res) => {
  console.log(req.query); 
  res.send('payment failed please check your credentials'); 
})

var createPay = ( payment ) => {
  return new Promise( ( resolve , reject ) => {
      paypal.payment.create( payment , function( err , payment ) {
       if ( err ) {
           reject(err); 
       }
      else {
          resolve(payment); 
      }
      }); 
  });
}
module.exports = router;

var mongoose = require('mongoose');
  var Schema = mongoose.Schema;

  var templates = new Schema({
    name:String,
    subject: String,
    content:String,
    isdelete:{
      type:Number,
      default:0
    }},{timestamps:{createdAt:"created_at",updatedAt:"updated_at"}});

  module.exports = mongoose.model("emailTemplate", templates);
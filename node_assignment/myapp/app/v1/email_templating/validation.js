const { check, validationResult } = require('express-validator/check');
class validator{
    validateHandler( req ,res ,next){
    let errors = validationResult(req);
    if (errors.isEmpty()) {
        next();
    }else{
        return res.status(422).json({ errors: errors });
    }
};
  get addUserVlaidate(){
      return [
          check('name').not().isEmpty().withMessage("enter your name"),
          check('email').not().isEmpty().isEmail().withMessage("enter your email"),
          check('address').not().isEmpty().withMessage("enter your address")
      ]
  }
}
module.exports = validator;
var express = require('express');
var router = express.Router();
var swig = require('swig');
var dbcontroller = require('./controller');
var dbControllerObj = new dbcontroller();
/* GET home page. */
router.get('/', async function (req, res, next) {
  let data = await dbControllerObj.fetchValue();
  res.render('list_email_template.html', { data: data });
  console.log(data,"kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk")
});

router.get('/addemailtemplate', function (req, res, next) {
  res.render('add_email_template.html');
});

router.post('/', function (req, res, next) {
  dbControllerObj.insertvalue(req.body);
  res.redirect('/emailtemplate');
});

router.get('/edit/:id', (req, res, next) => {
  dbControllerObj.get_user_data(req.params.id).then((result) => {
    res.render('edit_list_template.html', { data: result });
  })
});
router.post('/edit', async (req, res, next) => {
  await dbControllerObj.update_user_data(req.body.id, req.body)
  let data = await dbControllerObj.fetchValue();
  res.render('list_email_template.html', { data: data });
});
router.get('/delete/:id', (req, res, next) => {
  dbControllerObj.delete_user_data(req.params.id).then((result) => {
    console.log(result);
    res.redirect('/emailtemplate');
  })
});

module.exports = router;

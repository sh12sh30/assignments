var mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
var Schema = mongoose.Schema;

var userDetail = new Schema({
  fullname: String,
  email: {
    type: String,
    unique: true
  },
  address: String,
  password: String,
  isdelete: {
    type: Number,
    default: 0
  },
  user_type: {
    type: String,
    default: 'user'
  }
});
userDetail.plugin(mongoosePaginate);
module.exports = mongoose.model("User", userDetail);
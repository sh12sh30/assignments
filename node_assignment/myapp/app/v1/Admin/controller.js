var mongoose = require('mongoose');
var nodemailer = require('nodemailer');
var passwordHash = require('password-hash');
var path = require('path');
let User = require('./model');
let emailTemplate = require('../email_templating/model');

class dataController {
  async matchValue(result) {
    try {
      console.log(result);
      var data = await User.findOne({ userName: result.username, password: result.password })
      console.log(data);
      if (data == null) {
        return Promise.reject()
      }
      else {
        return Promise.resolve(data)
      }
    }
    catch (err) {
      console.log(err)
    }

  }
  async addUser(data) {
    try {

      User.create(data).then(doc => {
        console.log(doc)
      }).catch(err => {
        console.error(err)
      });

    }
    catch (err) {
      console.log(err)
    }
  }

  async insertvalue(data) {
    try {
      var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
      var string_length = 8;
      var randomstring = '';
      for (var i = 0; i < string_length; i++) {
        var rnum = Math.floor(Math.random() * chars.length);
        randomstring += chars.substring(rnum, rnum + 1);
      }
      var hashedPassword = passwordHash.generate(randomstring);
      console.log(hashedPassword)
      data.password = hashedPassword;
      User.create(data).then(doc => {
        console.log(doc)
      }).catch(err => {
        console.error(err)
      });
      var dataContent = await emailTemplate.findOne({ subject: 'unique password'});
      console.log(dataContent,"thsisss is data contenent")
      
      var content = dataContent.content;
      content = content.replace('/username/', data.email)
      content = content.replace('/password/', randomstring)
      console.log("bsdjcbsdc=========",content)
      var transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 587,
        secure: false,
        auth: {
          user: 'bhrdwjshubham.ss@gmail.com',
          pass: '<(&#^B#@M)>'
        }

      });
      const mailOptions = {
        from: 'bhrdwjshubham.ss@gmail.com', // sender address
        to: 'shubham12812@gmail.com', // list of receivers
        subject: 'in am sending a email', // Subject line
        html: content
      };


      transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
          return console.log(error.message);
        }
        console.log('success');
      });
    }
    catch (err) {
      console.log(err)
    }
  }
  async fetchValue(data) {
    console.log(data,"give the no. of data sizes")
    try {
      // console.log(data,"heloooo")
      var pageArr = [];
      let pageNo = 1, size = 3;
      if (data.pageNo) {
        pageNo = parseInt(data.pageNo)
      }
      let skip = ((pageNo - 1) * size)
      let query = { isdelete: 0 }
      let userCount = await User.find({}).count();
      let totalPages = parseInt(userCount / size)
      for (var i=1; i <=totalPages; i++) {
        pageArr.push(i);

      }
      if (userCount % size != 0) {
        totalPages++;
      }

      let result = await User.find({isdelete:0}).skip(skip).limit(size);
      // result.current = pageNo
      // if (pageNo == 1) {
      //   result.prev = 1
      // }
      // else {
      //   result.prev = pageNo - 1
      // }
      var jj = {
        next: pageNo + 1,
        last: totalPages,
        count: userCount,
        sr: (pageNo - 1) * size + 1,
        pageNo:pageArr
      }
      // result.next = pageNo + 1
      // result.last = totalPages
      // result.count = userCount
      // result.sr = (pageNo - 1) * size + 1
      return Promise.resolve([result, jj]);

    } catch (error) {
      console.log(error)

    }
  }
  async get_user_data(id) {
    try {

      var data = await User.findOne({ _id: id });
      return Promise.resolve(data);

    }
    catch (err) {
      console.log(err)
    }

  }
  async view_user_data(id) {
    try {

      var data = await User.findOne({ _id: id });
      return Promise.resolve(data);

    }
    catch (err) {
      console.log(err)
    }

  }

  async update_user_data(id, data) {
    try {
      var data = await User.update({ _id: id }, data);
      return Promise.resolve(data);

    }
    catch (err) {
      console.log(err)
    }

  }
  async delete_user_data(id) {
    try {
      var data = await User.updateOne({ _id: id }, { $set: { isdelete: 1 } });
      return Promise.resolve(data);

    }
    catch (err) {
      console.log(err)
    }

  }
  async pagination(pageNo) {
    try {
      var pageNo = parseInt(pageNo);
      var size = 5;
      var query = {}
      if (pageNo < 0 || pageNo === 0) {
        response = { "error": true, "message": "invalid page number, should start with 1" };
        return res.json(response)
      }
      query.skip = size * (pageNo - 1)
      query.limit = size
      User.find({}, {}, query, function (err, data) {
        // Mongo command to fetch all data from collection.
        if (err) {
          response = { "error": true, "message": "Error fetching data" };
        } else {
          response = { "error": false, "message": data };
        }
        res.json(response);
      });

    } catch (err) {
      console.log(err, "error occured")

    }
  }
  async paypal(){

  }
  async stripe(){
    
  }

}
module.exports = dataController;
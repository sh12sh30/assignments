var express = require('express');
var router = express.Router();
var path = require('path');
var swig = require('swig');
let User = require('./model');
var passport = require('passport');
var LocalStrategy = require('passport-local');
var multer = require('multer');
const fs = require('fs');

var dbcontroller = require('./controller');
var dbControllerObj = new dbcontroller();
// var validate = require('./validation');
// var validateObj = new validate();

router.get('/', async function (req, res, next) {

  dbControllerObj.fetchValue(req.query).then((data) => {
    if (!req.query.pageNo) {
      res.render('list_user.html', { data: data[0], showList: data[1], message: req.flash() });

    }
    else {

      console.log("enetr==========")
      var tpl = swig.compileFile(path.join(__dirname, '../../../views/pagination_list_user.html'));
      var userlist = tpl({ dataa: data[0] });

      res.send(userlist);
    }
  }).catch((err) => {
    console.log(err, "error occured")
  })

});



router.get('/user_form', function (req, res, next) {
  res.render('adduser.html');
});
router.post('/', function (req, res, next) {
  dbControllerObj.addUser(req.body);
  res.redirect('/admin');
});
router.post('/save_user', function (req, res, next) {
  dbControllerObj.insertvalue(req.body);
  res.redirect('/');
});

router.get('/edit/:id', (req, res, next) => {
  dbControllerObj.get_user_data(req.params.id).then((result) => {
    res.render('edituser.html', { data: result });
  })
});
router.get('/view/:id', (req, res, next) => {
  dbControllerObj.view_user_data(req.params.id).then((result) => {
    res.send({ result });
  })
});
router.post('/edit', async (req, res, next) => {
  await dbControllerObj.update_user_data(req.body.id, req.body)
  console.log(req.body.id);
  let data = await dbControllerObj.fetchValue();
  res.render('list_user.html', { data: data });
});

router.post('/edit', function (req, res, next) {
  res.render('list_user.html', { data: [req.body] });
});

router.get('/delete/:id', (req, res, next) => {
  dbControllerObj.delete_user_data(req.params.id).then((result) => {
    console.log(result);
    res.redirect('/user');
  })
});

router.get('/user_profile', async function (req, res, next) {
  res.render('user_profile.html', { data: req.user });
  console.log(req.user, "akldfasdfajkshdfkh+++++++++4334325=====0000222===")
});


module.exports = router;

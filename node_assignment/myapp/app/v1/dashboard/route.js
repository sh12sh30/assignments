var express = require('express');
var router = express.Router();
var path = require('path');
const refUser = require("./controller");
const user = require("../user/model");
const objUser = new refUser();
var paypal = require('paypal-rest-sdk');
var thumb = require("node-thumbnail")
var abc;

var multer = require('multer');
var storage = multer.diskStorage({
   destination: (req, file, cb) => {
      cb(null, path.join(__dirname, '/../../../public/upload'));
   },
   filename: (req, file, cb) => {
      cb(null, Date.now() + file.originalname)
   }
})
const upload = multer({ storage: storage })


router.get('/', (req, res) => {
   objUser.countRecord().then(result => {
      console.log(result);
      res.render('dashboard.html', { active: 'dashboard', sidebar:req.user, count: result[0],postcount:result[1] })
   }).catch((err)=>{
      console.log(err,"error occured")
   });
});


router.get('/changeProfile', (req, res) => {
   res.render('changeProfile', { data: req.user })

});


router.post('/changeProfile', (req, res) => {
   console.log("sdfghjmk")
   console.log(req.body)
   objUser.updateAdminProfile(req.user, req.body).then(result => {

      console.log(result)
      res.redirect("/dashboard")
   }).catch(err => {
      //error handling
      res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
   });
});

router.post('/profileImage', (req, res) => {
   console.log(req.body)
   objUser.uploadAdminProfileImage(req.user, req.body).then(result => {

      //  console.log(result)
      res.redirect("/dashboard")
   }).catch(err => {
      //error handling
      res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
   });
});
router.get('/changepassword', (req, res) => {
   res.render('changepassword.html')

});

router.post('/forgetpassword', (req, res, next) => {
   objUser.changePassword(req.body).then(result => {
      res.send("successfull")
   }).catch(err => {
      //error handling
      res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
   });
});


router.get('/logout', (req, res) => {
   if (req.session) {
      req.session.destroy((err) => {
         if (err) {
            return next(err);
         }
         else {
            return res.redirect('/')
         }
      })

   }

});







router.get('/cancle', (req, res) => { res.send('cancled') });






module.exports = router;